<?php

namespace App\Http\Controllers;
session_start();
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Bet;
use App\Models\Betnumber;
use App\Models\Win;
use App\Models\Result;
use App\Models\Period;
use App\Models\Admin;
use App\Models\Bankdetails;
use App\Models\Paymentrequest;
use App\Models\Rechargerequest;
use App\Models\Commision;
use App\Models\Deposit;
use Image;
//hjfuhfughfhg

class Users extends Controller
{

    function saveBank(Request $request)
    {
        Bankdetails::insert(['bank'=>$request->bank,'ac'=>$request->ac,'ifsc'=>$request->ifsc,'name'=>$request->name,'phone'=>$request->user,'branch'=>$request->branch]);

        header('location:banking?type=Details%20set!');
    }

    function setWithdraw(Request $request)
    {
        $user = User::where('phone',$request->phone)->first();
        if($request->pass == $user->password){
            if($request->amount <= $user->wallet){
        Paymentrequest::insert(['phone'=>$request->phone,'amount'=>$request->amount,'bank'=>$request->bank]);
        
        User::where('phone',$request->phone)->update(['wallet'=>($user->wallet-$request->amount)]);
        header('location:withdrawalUser?type=Withdraw%20requested!');
        }
        else
        {
            header('location:withdrawalUser?type=Invalid%20amount!');
        }
        }
        else
        {
            header('location:withdrawalUser?type=Invalid%20password!');
        }
        
    }

    function AcceptWithdraw(Request $request)
    {
        $allReq = explode(',',$request->dat);

        foreach($allReq as $req)
        {
            Paymentrequest::where('id',$req)->update(['status'=>'approved']);
        }

        header('location:withdrawal');
    }

    function RejectWithdraw(Request $request)
    {
        $allReq = explode(',',$request->dat);

        foreach($allReq as $req)
        {
            Paymentrequest::where('id',$req)->update(['status'=>'rejected']);
        }

        header('location:withdrawal');
    }

    function UserRegistration(Request $request)
    {
        $user = User::where('phone',$request->phone)->first();

        if($user){
            header('location:register?type=User%20exists%20already!');
        }
        else
        {

            if(!empty($request->refer))
            {
                User::insert(['phone'=>$request->phone,'password'=>$request->pass,'referedBy'=>$request->refer,'referal'=>$this->generateRandomString()]);
            }
            else{
                User::insert(['phone'=>$request->phone,'password'=>$request->pass,'referal'=>$this->generateRandomString()]);
            }
            
            $user = User::where('phone',$request->phone)->first();
            $data = ['phone'=>$user->phone,'wallet'=>$user->wallet];
            session(['phone'=>$user->phone,'wallet'=>$user->wallet]);
            return view('colorpicker/user/layout/layout',$data);
        }

    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function AddUser(Request $request)
    {
        $user = User::where('phone',$request->phone)->first();

        if($user)
        {
            header('location:adduser?type=User%20exists%20already!');
        }
        else
        {
            
            User::insert(['phone'=>$request->phone,'password'=>$request->pass]);
            header('location:adduser?type=User%20added%20successfully!');
            
        }

    }

    function BlockUser(Request $request)
    {
        User::where('phone',$request->u)->update(['status'=>'blocked']);
        header('location:alluser');
    }

    function UnBlockUser(Request $request)
    {
        User::where('phone',$request->u)->update(['status'=>'unblocked']);
        header('location:alluser');
    }

    function addFund(Request $request)
    {
        $u = User::where('phone',$request->phone)->first();
        User::where('phone',$request->phone)->update(['wallet'=>($request->amount + $u->wallet)]);
        date_default_timezone_set("Asia/Kolkata");
        Deposit::insert(['phone'=>$request->phone,'amount'=>$request->amount,'created_at'=>date('d-m-Y H:i:s')]);
        header('location:dashboard');
    }

    function ShowMainGame(Request $request)
    {
        
        return view('colorpicker/user/layout/game');
    }

    function login(Request $request)
    {
        //dd($request);
        $user = User::where('phone',$request->phone)->first();

        if($user)
        {
            if($request->pass == $user->password && $request->phone == $user->phone)
            {
                $data = ['phone'=>$user->phone,'wallet'=>$user->wallet];
                session(['phone'=>$user->phone,'wallet'=>$user->wallet]);
                if(!empty($request->remember)){
                    setcookie("phone", $user->phone, time() + (86400 * 30), "/");
                    setcookie("pass", $user->password, time() + (86400 * 30), "/");
                }
                    
                
                
                return view('colorpicker/user/layout/layout',$data);

                
            }
            else
            {
                header('location:login?type=Invalid%20user%20details!');
            }
        }
        else
        {
             header('location:login?type=Invalid%20user!');
        }
    }

    function loginDirectly($data)
    {
        $user = User::where('phone',$data['phone'])->first();
         if($user)
        {
             
            if($data['pass'] == $user->password && $data['phone'] == $user->phone)
            {
                
                $dat = ['phone'=>$user->phone,'wallet'=>$user->wallet];
                session(['phone'=>$user->phone,'wallet'=>$user->wallet]);
                
                    //setcookie("phone", $user->phone, time() + (86400 * 30), "/");
                    //setcookie("pass", $user->password, time() + (86400 * 30), "/");
                
                    
                
                
                header('location:userDash');

                
            }
            else
            {
                header('location:login?type=Invalid%20user%20details!');
            }
        }
        else
        {
             header('location:login?type=Invalid%20user!');
        }
    }

    function SetBet(Request $request)
    {
        $user = User::where('phone',$request->user)->first();

        if($user->wallet >= $request->amt){
            $firstShare = ($request->amt * 30)/100;
            $secShare   = ($request->amt * 20)/100;
            $thirdShare = ($request->amt * 10)/100;

        Bet::insert(['phone'=>$request->user,'amount'=>$request->amt,'color'=>$request->col,'period'=>$request->period]);
        User::where('phone',$request->user)->update(['wallet'=>($user->wallet - $request->amt)]);
        $user = User::where('phone',$request->user)->first();
        if($user->referedBy != null)
        {
            $first = User::where('referal',$user->referedBy)->first();
            User::where('referal',$user->referedBy)->update(['wallet'=>($first->wallet + $firstShare)]);
            Commision::insert(['phone'=>$user->phone,'refer'=>$user->referedBy,'amount'=>$firstShare,'type'=>'given']);
            if($first->referedBy != null)
            {
                $second = User::where('referal',$first->referedBy)->first();
                User::where('referal',$first->referedBy)->update(['wallet'=>($second->wallet + $secShare)]);
                Commision::insert(['phone'=>$first->phone,'refer'=>$first->referedBy,'amount'=>$secShare,'type'=>'given']);
                if($second->referedBy != null)
                {
                    $third = User::where('referal',$second->referedBy)->first();
                    User::where('referal',$second->referedBy)->update(['wallet'=>($third->wallet + $thirdShare)]);
                    Commision::insert(['phone'=>$second->phone,'refer'=>$second->referedBy,'amount'=>$thirdShare,'type'=>'given']);
                }
                
            }
        }
        return response($user->wallet,200);
        }
        else
        {
            return response("declined",200);
        }

        
    }

    public function resizeImage(Request $request)
    {
	    
        $usrName = $request->tesla;
        //dd($request->Myfile);
        $imgFile = \Image::make($request->file("Myfile")->getRealPath())->save(public_path('uploads/'.$usrName.'.png'));

        //$imgFile->resize(150, 150, function ($constraint) {
        //    $constraint->aspectRatio();
        //})->save($destinationPath.'/'.$input['file']);

        //$destinationPath = public_path('/uploads');
        //$image->move($destinationPath, $input['file']);

        header('location:update_qr');
    }

    function RequestRecharge(Request $request)
    {
        Rechargerequest::insert(['phone'=>$request->phone,'amount'=>$request->amount]);
        header('location:recharge?type=Recharge%20request%20has%20been%20submitted!');
    }

    function SetBetNumber(Request $request)
    {
        $user = User::where('phone',$request->user)->first();

        if($user->wallet >= $request->amt){
        Betnumber::insert(['phone'=>$request->user,'amount'=>$request->amt,'number'=>$request->num,'period'=>$request->period]);
        User::where('phone',$request->user)->update(['wallet'=>($user->wallet - $request->amt)]);
        $user = User::where('phone',$request->user)->first();
        return response($user->wallet,200);
        }
        else
        {
            return response("declined",200);
        }

        
    }

    function checkAdmin(Request $request)
    {
        $admin = Admin::where('userName',$request->username)->first();

        if($admin)
        {
            if($admin->password == $request->password)
            {
                $_SESSION['admin']=$admin->userName;
                header('location:dashboard');
            }
            else
            {
                header('location:admin?type=Invalid%20password!');
            }
        }
        else
        {
            header('location:admin?type=Invalid%20user%20name!');
        }
    }

    

     function reload(Request $request)
     {
         $data = ['phone'=>$request->phone,'wallet'=>$request->wallet];
         return view('colorpicker/user/layout/game',$data);
     }

    function GetResult($per)
    {
        $allData = Bet::where('period',$per)->get();
        $green =0 ;
        $red = 0;
        $violet = 0;
        $winner = 'green';
        $even = [2,4,6,8];
        $odd = [1,3,5,7,9];
        $number = 0;
        foreach($allData as $bet)
        {
            if($bet->color == 'green')
            {
                $green += $bet->amount;
            }
            else if($bet->color == 'red')
            {
                $red += $bet->amount;
            }
            else if($bet->color == 'violet')
            {
                $violet += $bet->amount;
            }
        }

        if($green == $red && $red == $violet)
        {
            $list = ['green','red','violet'];
            $randCol = $list[rand(0,(count($list)-1))];

            if($randCol == 'green')
            {
                $winner = 'green';
                $number = $odd[rand(0,(count($odd)-1))];
            }
            else if($randCol == 'red')
            {
                $winner = 'red';
                $number = $even[rand(0,(count($even)-1))];
            }
            else if($randCol == 'violet')
            {
                $winner = 'violet';
                $number = 0;
            }
        }
        else{
            if($green < $red && $green < $violet)
            {
            $winner = 'green';
            $number = $odd[rand(0,(count($odd)-1))];
            }

        if($red < $green && $red < $violet)
        {
            $winner = 'red';
            $number = $even[rand(0,(count($even)-1))];
        }

        if($violet < $green && $violet < $red)
        {
            $winner = 'violet';
            $number = 0;
        }
        }

        

        $winners = Betnumber::where('period',$per)->get();

        foreach($winners as $win)
        {
            if($win->number == $number)
            {
                $allCols = Bet::where('period',$per)->where('phone',$win->phone)->get();

                foreach($allCols as $allCol){
                    if($allCol->color == $winner)
                    {
                        Win::insert(['period'=>$win->period,'color'=>$allCol->color,'amount'=>(($win->amount+$allCol->amount) * 2),'phone'=>$win->phone,'number'=>$number]);
                    $userData = User::where('phone',$win->phone)->first();
                    User::where('phone',$win->phone)->update(['wallet'=>($userData->wallet+(($win->amount+$allCol->amount) * 2))]);
                    }
                }

                    
                
            }
            
        }

        $result = Result::where('period',$per)->first();
       $wins = Win::where('period',$per)->get();
       $totalWonAmt = 0;
       foreach($wins as $win)
       {
           $totalWonAmt += $win->amount;
       }
        if(!$result)
        {
            Result::insert(['period'=>$per,'number'=>$number,'color'=>$winner,'amount'=>$totalWonAmt]);
        }
        $periodNew = rand(100000000,999999999);
        Period::insert(['period'=>$periodNew]);
    }
}
