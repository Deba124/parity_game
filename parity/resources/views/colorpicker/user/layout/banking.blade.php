<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Bonus</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="../assets/css/all.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
  <script>
        function getParams() {
			var constQuery = window.location.search;
			const urlParams = new URLSearchParams(constQuery);
			var data = urlParams.get("type");
			console.log("data is" + data);
			if (data != "general" && data != null) {
				alert(data);
            }
        }
    </script>
 
</head>
<body onload="getParams()">
    
    <div class="container-fluid form-control form-group" style="background: #FF6700;box-shadow: 10px 5px 5px grey;">
  <span class="login"onclick="window.location.href='<?php echo 'userDash';?>'">&#8592; Banking Details </span>
     
  </div>
   <div class="container mt-5">
  <form action="saveBank" method="post">
      {{ csrf_field() }}
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Holder Name</label>
    <div class="col-sm-10">
      <input type="text" name="name" class="form-control" id="inputPassword" placeholder="Name" required>
    
    </div>
  </div>
      <input type="hidden" value="<?php echo(session('phone'));?>" name="user" />
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Ifsc Code</label>
    <div class="col-sm-10">
      <input type="text" name="ifsc" class="form-control" id="inputPassword" placeholder="Ifsc Code" required>
    </div>
  </div>
     <div class="form-group row">
     <label for="inputPassword" class="col-sm-2 col-form-label">Bank Name</label>
     <div class="col-sm-10">
      <input type="text" name="bank" class="form-control" id="inputPassword" placeholder="Bank Name" required>
    </div>
  </div>
    <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Bank Account Number </label>
    <div class="col-sm-10">
      <input type="text" name="ac" class="form-control" id="inputPassword" placeholder="Bank Account" required>
    </div>
  </div>
  
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Branch Name</label>
    <div class="col-sm-10">
      <input type="text" name="branch" class="form-control" id="inputPassword" placeholder="Branch Name">
    </div>
  </div>
      <center><button class="copy_link1 mt-5">Submit</button></center>
</form>
   
    <br/>
       <br/>
       <table class="table table-bordered">
           <thead>
               <tr>
                   <th>
                       Bank
                   </th>
                   <th>
                       Account No.
                   </th>
                   <th>
                       Ifsc
                   </th>
                   <th>
                       Branch
                   </th>
                   <th>
                       A/C Holder Name
                   </th>
               </tr>
           </thead>
           <tbody>
               <?php
         use App\Models\Bankdetails;

         $banks = Bankdetails::where('phone',session('phone'))->get();

         foreach($banks as $bank)
         {
             echo("<tr><td>".$bank->bank."</td><td>".$bank->ac."</td><td>".$bank->ifsc."</td><td>".$bank->branch."</td><td>".$bank->name."</td></tr>");
         }
         ?>
           </tbody>
       </table>

   </div>
            
</body>

</html>
