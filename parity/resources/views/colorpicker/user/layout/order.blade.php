<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Win Money</title>

	<link rel="stylesheet" type="text/css" href="{{URL::asset('css/all.css')}}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div class="container-fluid form-control form-group" style="background: #FF6700;box-shadow: 10px 5px 5px grey;">
	<span class="login"onclick="window.location.href='<?php echo 'userDash';?>'"> &#8592;My Order</span>
	</div>
	 <div class="container">
	 	 <div class="col-md-12 order">
	 	 	

			  <table class="table  table-bordered text-center mt-5">
	 	<thead>
	 		<tr>
	 			<th>Period</th>
	 			<th>Time</th>
	 			<th>Combinations</th>
	 			<th>Status</th>
	 		</tr>
			 </thead>
	 		<tbody>
	 			<?php
				 use App\Models\Period;
				 use App\Models\Bet;
				 use App\Models\Betnumber;
				 use App\Models\Win;
				 $periods = Period::select('*')->get();
				 $totalBetAmt = 0;
				 $totalWonAmt = 0;
				 foreach($periods as $period)
                 {
					 $allBets = Bet::where('period',$period->period)->where('phone',session('phone'))->get();
					 $allBetsNum = Betnumber::where('period',$period->period)->where('phone',session('phone'))->get();
					 if(count($allBets)>0)
                     {
                        echo("<tr><td>".$period->period."</td><td>".$period->created_at."</td>");
					 echo("<td>");
					 foreach($allBets as $allBet)
                     {
                         echo($allBet->color.",");
						 $totalBetAmt += $allBet->amount;
                     }
					 foreach($allBetsNum as $allBetnum)
                     {
                         echo($allBetnum->number.",");
						 $totalBetAmt += $allBet->amount;
                     }
					 echo("</td>");

					 $wonBet = Win::where('period',$period)->where('phone',session('phone'))->first();
					 if($wonBet)
                     {
                         $totalWonAmt = $wonBet->amount;
						 if($totalWonAmt >= $totalBetAmt)
                         {
                             echo("<td>Profit</td></tr>");
                         }
						 else
                         {
                             echo("<td>Loss</td></tr>");
                         }
                     }
					 else
                         {
                             echo("<td>Loss</td></tr>");
                         }
                     }
					 
                     
                 } ?>
				 
	 		</tbody>
	 	
	 </table>
	 	 	
	 	 </div>
	 </div>

	</body>
</html>
