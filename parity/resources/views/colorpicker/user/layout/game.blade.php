<!DOCTYPE html>
<html>
<head>
	   <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	   <title>Game</title>
     <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin.css');}}">
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script>
        var colorType;
        var contract;
        var totalBetAmount;
        var numberCount = 0;
        var numberType = -1;
        var Time = 180;

        function Timer() {
            var min = 2;
            var sec = 59;
             window.setInterval(function(){
  /// call your function here
                 
                 if(sec>9)
                     document.getElementById('app').innerHTML = "0" + min + ":" + sec;
                 else
                     document.getElementById('app').innerHTML = "0" + min + ":0" + sec;
                 Time--;
                 sec--;
                 if (sec < 0) {
                     min--;
                     sec = 59;
                 }
            if (Time <= 0) {
                window.location.href = "game";
            }
}, 1000);
        }
        

        function startTimer() {
           
        }

        function SelectedColor(color) {
            resetAll();
            colorType = color;
            
        }

        function confirmBet() {

            if (Time <= 30) {
                alert("sorry betting is closed!");
                return;
            }

            if (colorType == "green") {
                
                submitDetails("green", totalBetAmount);
            }
            else if (colorType == "violet") {
                
                submitDetails("violet", totalBetAmount);
            }
            else if (colorType == "red") {
                
                submitDetails("red", totalBetAmount);
            }
        }

        function confirmBetNum() {
            if (Time <= 30) {
                alert("sorry betting is closed!");
                return;
            }
            submitDetailsNum(numberType, totalBetAmount);
        }

        function submitDetails(colorName, amount) {
             var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if (this.responseText == "declined") {
              alert("Invalid amount!");
          }
          else {
              document.getElementById("bal").innerHTML = this.responseText;
          }
     
    }
            };
            xhttp.open("GET", "submitGame?col=" + colorName + "&amt=" + amount + "&user=" + document.getElementById("udata").innerHTML +"&period="+document.getElementById("peri").innerHTML, true);

            xhttp.send();
        }

        function submitDetailsNum(numberName, amount) {
             var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          if (this.responseText == "declined") {
              alert("Invalid amount!");
          }
          else {
              document.getElementById("bal").innerHTML = this.responseText;
          }
     
    }
            };
            xhttp.open("GET", "submitGameNum?num=" + numberName + "&amt=" + amount + "&user=" + document.getElementById("udata").innerHTML +"&period="+document.getElementById("peri").innerHTML, true);

            xhttp.send();
        }

        function addContract(amount) {
            contract = amount;

        }

        function resetAll() {
            colorType = "";
            contract = 0;
            totalBetAmount = 0;
            numberCount = 0;
            numberType = -1;
            document.getElementById("incrGreen").innerHTML = 0;
            document.getElementById("incrViolet").innerHTML = 0;
            document.getElementById("incrRed").innerHTML = 0;
            document.getElementById("incrNum").innerHTML = 0;
        }
        
        function IncrementNumber() {

            totalBetAmount += contract;
            numberCount++;
            if (colorType == "green")
                document.getElementById("incrGreen").innerHTML = numberCount;
            else if (colorType == "violet")
                document.getElementById("incrViolet").innerHTML = numberCount;
            else if (colorType == "red")
                document.getElementById("incrRed").innerHTML = numberCount;
            else if (numberType > -1) {
                document.getElementById("incrNum").innerHTML = numberCount;
            }
           
        }
        function DecrementNumber() {

            if (numberCount < 0) {
                return;
            }

            totalBetAmount -= contract;
            numberCount--;
            if (colorType == "green")
                document.getElementById("incrGreen").innerHTML = numberCount;
            else if (colorType == "violet")
                document.getElementById("incrViolet").innerHTML = numberCount;
            else if (colorType == "red")
                document.getElementById("incrRed").innerHTML = numberCount;
           else if (numberType > -1) {
                document.getElementById("incrNum").innerHTML = numberCount;
            }
            
        }

        function SetNumber(number) {
            resetAll();
            document.getElementById('exampleModalLongTitleNum').innerHTML = "Select "+number;
            numberType = number;
        }
        
        
    </script>
    
</head>
<body onload="Timer()">
    <?php
    use App\Models\User;
    $userData = User::where('phone',session('phone'))->first();
    ?>
    <p id="udata" style="display:none;">{{$userData->phone}}</p>
    <div class="container-fluid">
      <div class="gameheader">
      	 <div class="gameheader1 p-3">Available Balance : ₹ <strong id="bal">{{$userData->wallet}}</strong> </div>     
          <a href="recharge" class="btn btn-success"> Recharge</a>
     
    </div>
 </div>
      <center><h3>Trade</h3></center>
     <hr style="background: #FF6700;height:2px;"> 
  <div class="container-fluid"> 
       
          <div  class="float-left">
              <i class="fas fa-trophy"></i>Period
              <div class="trophy" id="peri"> <?php
                                             use App\Models\Period;
                                             $dat = Period::latest()->first();
                                             echo($dat->period);
                                             ?></div>
          </div>
          <div  class="float-right"><p>Countdown</p>
              <div  id="app" class="trophy1 float-right"></div>
          </div>
     
  </div>
 <div class="join">
  <div class="container-fluid">
  
      <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 float-left">
               <!-- Button trigger modal -->
               <button onclick="SelectedColor('green')" type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModalCenter">
                 Join Green
               </button>

               <!-- Modal -->
               <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header bg-success">
                      <h5 class="modal-title text-white" id="exampleModalLongTitle">Join Green</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Contract Money <br>
                        <ul class="nav nav-pills">
                            <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(10)" href="#">10</a></li> &nbsp&nbsp
                            <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(100)" href="#">100</a></li> &nbsp&nbsp
                             <li> <a data-toggle="pill" class="btn btn-info" onclick="addContract(1000)" href="#">1000</a></li> &nbsp&nbsp
                             <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(10000)" href="#">10000</a></li>
                        </ul>
                        
                        
                   
                    
                      <br>
                      Number of tickets <br>
                      <button onclick="DecrementNumber()" class="p-2">—</button>
                       <h1 style="display:inline; text-align:center; float:initial;" id="incrGreen">0</h1>
                     <button onclick="IncrementNumber()" class="p-2 float-right">&#43;</button>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                      <button onclick="confirmBet()" data-dismiss="modal" type="button" class="btn btn-primary">Confirm</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 text-center">
              <button type="button" onclick="SelectedColor('violet')" class="btn btn-success btn_modal" data-toggle="modal" data-target="#exampleModalCenter1" style="background: #8F00FF;">
                Join violet 
              </button>
            </div>

            <div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header"style="background: #8F00FF;">
                    <h5 class="modal-title text-white" id="exampleModalLongTitle">Join Violet</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                     Contract Money <br>
                     <ul class="nav nav-pills">
                            <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(10)" href="#">10</a></li> &nbsp&nbsp
                            <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(100)" href="#">100</a></li> &nbsp&nbsp
                             <li> <a data-toggle="pill" class="btn btn-info" onclick="addContract(1000)" href="#">1000</a></li> &nbsp&nbsp
                             <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(10000)" href="#">10000</a></li>
                        </ul>
                      <br>
                      Number of tickets <br>
                      <button onclick="DecrementNumber()" class="p-2">—</button>
                       <h1 style="display:inline; text-align:center; float:initial;" id="incrViolet">0</h1>
                     <button onclick="IncrementNumber()" class="p-2 float-right">&#43;</button>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" onclick="confirmBet()" data-dismiss="modal" class="btn btn-primary">Confirm</button>
                  </div>
                </div>
              </div>
            </div> 
        <div class="col-md-4 text-right">
         <button onclick="SelectedColor('red')" type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter2">
           Join Red
         </button>

         <!-- Modal -->
         <div class="modal fade text-left" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header bg-danger text-white">
                <h5 class="modal-title" id="exampleModalLongTitle">Join Red</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
               contract Money <br>
                    <ul class="nav nav-pills">
                            <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(10)" href="#">10</a></li> &nbsp&nbsp
                            <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(100)" href="#">100</a></li> &nbsp&nbsp
                             <li> <a data-toggle="pill" class="btn btn-info" onclick="addContract(1000)" href="#">1000</a></li> &nbsp&nbsp
                             <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(10000)" href="#">10000</a></li>
                        </ul>
                      <br>
                      Number <br>
                      <button onclick="DecrementNumber()" class="p-2">—</button>
                       <h1 style="display:inline; text-align:center; float:initial;" id="incrRed">0</h1>
                     <button onclick="IncrementNumber()" class="p-2 float-right">&#43;</button>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" onclick="confirmBet()" data-dismiss="modal" class="btn btn-primary">Confirm</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="container-fluid mt-5">
   <div class="row">
     <div class="col-md-2">
     
         <div class="modal fade text-left" id="exampleModalCenterNum" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header bg-primary text-white">
                <h5 class="modal-title" id="exampleModalLongTitleNum"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
               contract Money <br>
                    <ul class="nav nav-pills">
                            <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(10)" href="#">10</a></li> &nbsp&nbsp
                            <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(100)" href="#">100</a></li> &nbsp&nbsp
                             <li> <a data-toggle="pill" class="btn btn-info" onclick="addContract(1000)" href="#">1000</a></li> &nbsp&nbsp
                             <li><a data-toggle="pill" class="btn btn-info" onclick="addContract(10000)" href="#">10000</a></li>
                        </ul>
                      <br>
                      Number <br>
                      <button onclick="DecrementNumber()" class="p-2">—</button>
                  <h1 style="display:inline; text-align:center; float:initial;" id="incrNum">0</h1>
                     <button onclick="IncrementNumber()" class="p-2 float-right">&#43;</button>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" onclick="confirmBetNum()" data-dismiss="modal" class="btn btn-primary">Confirm</button>
              </div>
            </div>
          </div>
        </div>
     </div>
       <div class="col-md-2">
           <button onclick="SetNumber(0)" class="btn btn btn-primary btnfirst "data-toggle="modal" data-target="#exampleModalCenterNum">&nbsp&nbsp&nbsp&nbsp0&nbsp&nbsp&nbsp</button>
       </div>
     <div class="col-md-2">
         
       <button onclick="SetNumber(1)" class="btn btn btn-primary btnfirst1"data-toggle="modal" data-target="#exampleModalCenterNum">&nbsp&nbsp&nbsp1&nbsp&nbsp&nbsp</button>
       
     </div>
     <div class="col-md-2">
       <button onclick="SetNumber(2)" class="btn btn btn-primary btnfirst2"data-toggle="modal" data-target="#exampleModalCenterNum">&nbsp&nbsp&nbsp2&nbsp&nbsp&nbsp</button>
       
     </div>
     <div class="col-md-2">
       <button onclick="SetNumber(3)" class="btn btn btn-primary btnfirst3"data-toggle="modal" data-target="#exampleModalCenterNum">&nbsp&nbsp&nbsp3&nbsp&nbsp&nbsp</button>
        
     </div>
     <div class="col-md-2">
       <button onclick="SetNumber(4)" class="btn btn btn-primary btnfirst4"data-toggle="modal" data-target="#exampleModalCenterNum">&nbsp&nbsp&nbsp4&nbsp&nbsp&nbsp</button>
       
     </div>
   </div>
 </div>
  <div class="container-fluid mt-5">
   <div class="row">
     <div class="col-md-2">
     <button onclick="SetNumber(5)" class="btn btn btn-primary btnfirst"data-toggle="modal" data-target="#exampleModalCenterNum">&nbsp&nbsp&nbsp5&nbsp&nbsp&nbsp</button>
     
     </div>
     <div class="col-md-2">
       <button onclick="SetNumber(6)" class="btn btn btn-primary btnfirst1"data-toggle="modal" data-target="#exampleModalCenterNum">&nbsp&nbsp&nbsp6&nbsp&nbsp&nbsp</button>
       
     </div>
     <div class="col-md-2">
       <button onclick="SetNumber(7)" class="btn btn btn-primary btnfirst2"data-toggle="modal" data-target="#exampleModalCenterNum">&nbsp&nbsp&nbsp7&nbsp&nbsp&nbsp</button>
       
     </div>
     <div class="col-md-2">
         <button onclick="SetNumber(8)" class=" btn btn-primary btnfirst3"data-toggle="modal" data-target="#exampleModalCenterNum">&nbsp&nbsp&nbsp8&nbsp&nbsp&nbsp</button>
     
     </div>
     <div class="col-md-2">
       <button onclick="SetNumber(9)" class="btn btn btn-primary btnfirst4" data-toggle="modal" data-target="#exampleModalCenterNum">&nbsp&nbsp&nbsp9&nbsp&nbsp&nbsp</button>
       
     </div>
   </div> 
</div>
   <div class="container-fluid mt-5">
      <div class="text-center">
           <i class="fas fa-trophy"></i><br><h4>Trade Record</h4>
      </div>
       <hr style="background: #FF6700;height:2px; box-shadow: 10px 5px 5px grey;"> 
   </div>
   <div class="container-fluid">
       <table class="table table-striped">
          <thead>
             <tr>
               <th>Period</th>
               
               <th>Number</th>
               <th>Result</th>
             </tr>
          </thead>
          <tbody>
               <?php
               use App\Models\Result;
               $results = Result::select('*')->orderBy('created_at','desc')->get();
               foreach($results as $res)
               {
                   echo("<tr><td>".$res->period."</td><td>".$res->number."</td>");
                   if($res->color == "green")
                   {
                       echo("<td><span style='background-color:green; border-radius:50%; height: 25px; width: 25px; display:inline-block;'></span>");
                   }
                   else if($res->color == "red")
                   {
                       echo("<td><span style='background-color:red; border-radius:50%; height: 25px; width: 25px; display:inline-block;'></span>");
                   }
                   else if($res->color == "violet")
                   {
                       echo("<td><span style='background-color:violet; border-radius:50%; height: 25px; width: 25px; display:inline-block;'></span>");
                   }

                   if($res->number == 0)
                   {
                       echo("&nbsp&nbsp<span style='background-color:red; border-radius:50%; height: 25px; width: 25px; display:inline-block;'></span>");
                   }
                   else if($res->number == 5)
                   {
                       echo("&nbsp&nbsp<span style='background-color:violet; border-radius:50%; height: 25px; width: 25px; display:inline-block;'></span>");
                   }

                   echo("</td></tr>");
               }
               ?>
          </tbody>
       </table>
   </div>
        <div class="container-fluid mt-5">
      <div class="text-center">
           <i class="fa fa-sticky-note"></i><br><h4>My Record</h4>
      </div>
       <hr style="background: #FF6700;height:2px; box-shadow: 10px 5px 5px grey;"> 
       <p style="text-align:center;">There is no unsettled order at present. If you want to query the settled order, please go to the order record to query</p>
        <hr style="background: #FF6700;height:2px; box-shadow: 10px 5px 5px grey;"> 
       <a href="order"><center><button class="btn btn-success">Myorder</button></center> </a>
   </div>
</body>
<script type="text/javascript">
// Credit: Mateusz Rybczonec

const FULL_DASH_ARRAY = 283;
const WARNING_THRESHOLD = 10;
const ALERT_THRESHOLD = 5;

const COLOR_CODES = {
  info: {
    color: "green"
  },
  warning: {
    color: "orange",
    threshold: WARNING_THRESHOLD
  },
  alert: {
    color: "red",
    threshold: ALERT_THRESHOLD
  }
};

const TIME_LIMIT = 180;
let timePassed = 0;
let timeLeft = TIME_LIMIT;
let timerInterval = null;
let remainingPathColor = COLOR_CODES.info.color;

document.getElementById("app").innerHTML = `
<div class="base-timer">
  <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
    <g class="base-timer__circle">
      <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45"></circle>
      <path
        id="base-timer-path-remaining"
        stroke-dasharray="283"
        class="base-timer__path-remaining ${remainingPathColor}"
        d="
          M 50, 50
          m -45, 0
          a 45,45 0 1,0 90,0
          a 45,45 0 1,0 -90,0
        "
      ></path>
    </g>
  </svg>
  <span id="base-timer-label" class="base-timer__label">${formatTime(
    timeLeft
  )}</span>
</div>
`;

startTimer();

function onTimesUp() {
  clearInterval(timerInterval);
}

function startTimer() {
  timerInterval = setInterval(() => {
    timePassed = timePassed += 1;
    timeLeft = TIME_LIMIT - timePassed;
    document.getElementById("base-timer-label").innerHTML = formatTime(
      timeLeft
    );
    setCircleDasharray();
    setRemainingPathColor(timeLeft);

    if (timeLeft === 0) {
      onTimesUp();
    }
  }, 1000);
}

function formatTime(time) {
  const minutes = Math.floor(time / 60);
  let seconds = time % 60;

  if (seconds < 10) {
    seconds = `0${seconds}`;
  }

  return `${minutes}:${seconds}`;
}

function setRemainingPathColor(timeLeft) {
  const { alert, warning, info } = COLOR_CODES;
  if (timeLeft <= alert.threshold) {
    document
      .getElementById("base-timer-path-remaining")
      .classList.remove(warning.color);
    document
      .getElementById("base-timer-path-remaining")
      .classList.add(alert.color);
  } else if (timeLeft <= warning.threshold) {
    document
      .getElementById("base-timer-path-remaining")
      .classList.remove(info.color);
    document
      .getElementById("base-timer-path-remaining")
      .classList.add(warning.color);
  }
}

function calculateTimeFraction() {
  const rawTimeFraction = timeLeft / TIME_LIMIT;
  return rawTimeFraction - (1 / TIME_LIMIT) * (1 - rawTimeFraction);
}

// function setCircleDasharray() {
//   const circleDasharray = `${(
//     calculateTimeFraction() * FULL_DASH_ARRAY
//   ).toFixed(0)} 283`;
//   document
//     .getElementById("base-timer-path-remaining")
//     .setAttribute("stroke-dasharray", circleDasharray);
// }
</script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript">
    $('#first').on('click',function(){
      var a=$('#10s').val();
      // 
      var b=$
    });
  </script>
 
</html>
