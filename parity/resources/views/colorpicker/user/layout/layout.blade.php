<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Dashboard</title>
	 <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Win Money</title>

	<link rel="stylesheet" type="text/css" href="{{URL::asset('css/all.css');}}"/>
	
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
 <style>
   .user_details
   {
   margin-top: -70px;
   margin-left: 60px;
   }
   <style>
body {
  font-family: "Lato", sans-serif;
}


.wrapper {
  display: flex;
  text-decoration: none;
  transition: all 0.4s;
}

#sidebar {

  color: #fff;
  transition: all 0.4s;
 height: 70vh;
}

#sidebar.active {
  margin-left: -250px;
}

#sidebar .sidebar-header {
  padding: 20px;
 
}

#sidebar ul.components {
  padding: 20px 0;
  /* border-bottom: 1px solid #47748b; */
}

#sidebar ul p {
  color: #fff;
  padding: 10px;
}

#sidebar ul li a {
  padding: 10px;
  font-size: 25px;
  display: block;
  color: black;
  text-decoration: none;
}

#sidebar ul li.active>a,
a[aria-expanded="true"] {
  color: black;

}

a[data-toggle="collapse"] {
  position: relative;
}

.dropdown-toggle::after {
  display: block;
  position: absolute;
  top: 50%;
  right: 20%;
  transform: translateY(-50%);
}

ul ul a {
  font-size: 20px;
  padding-left: 30px !important;
 
}
.footer
	{
   background: #FF6700;
	}
</style>
    <script type="text/javascript">
        $(document).ready(function () {
  $('#msbo').on('click', function(){
    $('body').toggleClass('msb-x');
  });
});
</script>
 </head>
<body>
    <?php
    use App\Models\User;
    $user = User::where('phone',session('phone'))->first();
    ?>
   <div class="container-fluid head" style="background-color:orangered;">
   	<div class="header">
   	<div class="user">
   	<i class="fa fa-user-alt mt-2 mr-3" style="font-size: 60px;color: white;"></i>
     </div>
     <div class="user_details">
     	 <span class="ml-2 mt-1">User:<?php echo($user->phone);?><span><br>
   	 <span  class="ml-2 mt-1">Id:<?php echo($user->id);?></span>
     </div>
     <center>
        <img src="{{URL::asset('images/aa.png');}}" style="width:70px;margin-top: -90px;">
     </center>
   	 <div class="float-right bell">
   	 	<i class="fas fa-bell mb-4"></i>
   	 </div>
   	</div> 
   	 <div class="container ">
   	   <div class="col-md-12"> 
   	 	  <div class="row">
   	 	  	<div class="col-md-6 recharge mb-3 recharge">
   	 	  	<center> <span><?php echo($user->wallet);?></span><br>
   	 	  		 <span>Balance</span><br>
   	 	  		 <a onclick="window.location.href='<?php echo 'recharge';?>'"><button class="recharge">Recharge</button></a></center>	
   	 	  	</div>
   	 	  	 <div class="col-md-6 mb-3 sell ">
   	 	        <center>
   	 	  	    <span>0.00</span><br>
   	 	  		 <span>Commission</span><br>
   	 	  		<a onclick="window.location.href='<?php echo 'commision';?>'" ><button class="sell">See</button></a>
   	 	  	</center>	 
   	 	  	</div>
   	 	  	  <!-- <div class="col-md-4 mb-3 sell1">
   	 	  	  <center>	
   	 	  		 <span>0.00</span><br>
   	 	  		 <span>Interest</span><br>
   	 	  	   <a onclick="window.location.href='<?php echo 'interest';?>'"><button class="sell">See</button></a>
   	 	  	  </center>	 
   	 	  	</div> -->
   	 	  </div>
   	 	
   	 </div>
   	</div> 
   </div>

    <!--Side bar-->
    <div class="wrapper">
  <nav id="sidebar">

    <ul class="lisst-unstyled components ">
        <li>
        <a href="order"><i class="fab fa-first-order-alt mr-2"></i>Order</a>
      </li>
       <li>
        <a href="referral"><i class="fas fa-sync mr-2"></i>Referral</a>
      </li>
      <li class="active">
          <div class="panel panel-default">
        <a href="#foodSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-wallet mr-2" ></i>Wallet</a>
          <div id="foodSubmenu" class="panel-collapse collapse">
              <ul class="">
          <li><a href="recharge"><i class="fas fa-recycle mr-2"></i>Recharge</a></li>
          <li><a href="transactionUser"><i class="fas fa-file-invoice mr-2"></i>Deposits</a></li>
          <li><a href="withdrawalUser"><i class="far fa-sticky-note mr-2"></i>Withdrawal</a></li>
        </ul>
          </div>
        </div>
      </li>
        
      <li>
        <a href="banking"><i class="fas fa-calendar-week mr-2"></i>Banking Details</a>
      </li>
      <li>
        <a href="https://api.whatsapp.com/send?phone=7509832073"><i class="fas fa-headset mr-2"></i>Support</a>
      </li>
    </ul>
  </nav>
  <div id="content">
    <!-- <a href="https://www.jqueryscript.net/tags.php?/Navigation/">Navigation</a> Toggler -->
    <nav class="navbar navbar-expand-lg">
      <div class="container-fluid">
     
      </div>
    </nav>
    <!-- Main Content Here -->
  </div>
</div>
    <!--End side bar-->

    <!--FooterStart-->
    <div class="footer">
  <div class="container mr-2">
  	<div class="col-md-12 ">
  		<div class="row">		
  			<div class="col-md-6 float-right ">
  			 <div class="ml-5">
  			<a href="game?phone=<?php echo($user->phone);?>&wallet=<?php echo($user->wallet);?>">	
  			 <i class="fas fa-trophy mt-3 ml-1" style="color:white;font-size:30px;"></i><br>
  				<h6 style="color:white;font-size:20px;text-decoration: none;">Play</h6>
  				</a>
  				 </div>
  			</div>
  			<div class="col-md-6 ">
  				<div class="ml-5">
  				<a href="layout.php">
  				 <i class="fas fa-user mt-3" style="color:white;font-size:30px;"></i><br>
  				  <h6  style="color:white;font-size:20px;text-decoration:none;">My</h6>
  				 </a> 
  				</div>
  			</div>
  		</div>
  	</div>
  </div>
</div>
    <!--End footer-->
</body>
</html>
