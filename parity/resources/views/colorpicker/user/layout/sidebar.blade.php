<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>sidebar</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/all.css">
	
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
</head>

<style>
body {
  font-family: "Lato", sans-serif;
}


.wrapper {
  display: flex;
  text-decoration: none;
  transition: all 0.4s;
}

#sidebar {

  color: #fff;
  transition: all 0.4s;
 height: 70vh;
}

#sidebar.active {
  margin-left: -250px;
}

#sidebar .sidebar-header {
  padding: 20px;
 
}

#sidebar ul.components {
  padding: 20px 0;
  /* border-bottom: 1px solid #47748b; */
}

#sidebar ul p {
  color: #fff;
  padding: 10px;
}

#sidebar ul li a {
  padding: 10px;
  font-size: 25px;
  display: block;
  color: black;
  text-decoration: none;
}

#sidebar ul li.active>a,
a[aria-expanded="true"] {
  color: black;

}

a[data-toggle="collapse"] {
  position: relative;
}

.dropdown-toggle::after {
  display: block;
  position: absolute;
  top: 50%;
  right: 20%;
  transform: translateY(-50%);
}

ul ul a {
  font-size: 20px;
  padding-left: 30px !important;
 
}

</style>
</head>
<body>

    <div class="wrapper">
  <nav id="sidebar">

    <ul class="lisst-unstyled components ">
        <li>
        <a href="order.php"><i class="fab fa-first-order-alt mr-2"></i>Order</a>
      </li>
       <li>
        <a href="referral.php"><i class="fas fa-sync mr-2"></i>Referral</a>
      </li>
      <li class="active">
        <a href="#foodSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-wallet mr-2" ></i>Wallet</a>
        <ul class="collapse lisst-unstyled" id="foodSubmenu">
          <li><a href="recharge.php"><i class="fas fa-recycle mr-2"></i>Recharge</a></li>
          <li><a href="transaction.php"><i class="fas fa-file-invoice mr-2"></i>Transaction</a></li>
          <li><a href="withdrawal.php"><i class="far fa-sticky-note mr-2"></i>Withdrawal</a></li>
        </ul>
      </li>
      <li>
        <a href="banking.php"><i class="fas fa-calendar-week mr-2"></i>Banking Details</a>
      </li>
      <li>
        <a href="https://api.whatsapp.com/send?phone=7509832073"><i class="fas fa-headset mr-2"></i>Support</a>
      </li>
    </ul>
  </nav>
  <div id="content">
    <!-- <a href="https://www.jqueryscript.net/tags.php?/Navigation/">Navigation</a> Toggler -->
    <nav class="navbar navbar-expand-lg">
      <div class="container-fluid">
     
      </div>
    </nav>
    <!-- Main Content Here -->
  </div>
</div>
</body>
<script type="text/javascript">
functi(){
  $('#msbo').on('click', function(){
    $('body').toggleClass('msb-x');
  });
}();
</script>
<!-- CSS only -->

<!-- JavaScript Bundle with Popper -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</html>
