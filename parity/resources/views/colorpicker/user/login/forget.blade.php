<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Forget Password</title>

	<link rel="stylesheet" type="text/css" href="{{URL::asset('css/all.css');}}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
     <div class="container-fluid form-control form-group" style="background: #FF6700;box-shadow: 10px 5px 5px grey;">
	<span class="login"><span type="btn" onclick="window.location.href='login';"> &#8592;<span> Reset Password</span>
	</div>
	<div class="container "style="margin-top: 50px;">
     <form>
        <div class="phone-user">
          <form>
        <div class="phone-user mt-3">
        <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><img src="{{URL::asset('images/phone.png');}}" height="30" width="30" class="img-fluid" style="background: #FF6700;box-shadow: 10px 5px 5px grey;"></span>
         </div>
         <input type="text" name="phone" class="form-control" placeholder="Please Enter a Mobile" aria-label="Username" aria-describedby="basic-addon1" style="box-shadow: 10px 5px 5px grey;"required>
         </div>
         </div>
         <div class="pass-user"> 
          <div class="input-group mb-3">
            <div class="input-group-prepend">
             <span class="input-group-text" id="basic-addon1"><img src="{{URL::asset('images/3.png');}}" class="img-fluid" style="box-shadow: 10px 5px 5px grey;" height="30" width="30"></span>
          </div>
          <input type="text" class="form-control" placeholder="Verification code" aria-label="Username" aria-describedby="basic-addon1" style="box-shadow: 10px 5px 5px grey;" required>
         </div>
       </div>
        
        <div class="Password"> 
          <div class="input-group mb-3">
            <div class="input-group-prepend">
             <span class="input-group-text" id="basic-addon1"><img src="{{URL::asset('images/9.jpg');}}" class="img-fluid" style="box-shadow: 10px 5px 5px grey;" height="30" width="30"></span>
          </div>
          <input name="pass" type="text" class="form-control" placeholder="Enter New Password" aria-label="Username" aria-describedby="basic-addon1" style="box-shadow: 10px 5px 5px grey;" required>
         </div>
       </div>
    
       <center><button type="submit" class="btn  mt-5 sub"><h3>Continue</h3></button></center>
      
</form>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>
