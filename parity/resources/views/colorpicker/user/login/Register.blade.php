<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Register</title>

	<link rel="stylesheet" type="text/css" href="{{URL::asset('css/all.css');}}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script>
		function getParams() {
			var constQuery = window.location.search;
			const urlParams = new URLSearchParams(constQuery);
			var data = urlParams.get("type");
			console.log("data is" + data);
			if (data != "general" && data != null) {
				alert(data);
            }
        }
	</script>

</head>
<body onload="getParams()">
  <div class="container-fluid form-control form-group" style="background: #FF6700;box-shadow: 10px 5px 5px grey;">
<span class="login"><span type="button" onclick="window.location.href='login';"> &#8592;</span> Register</span>
	</div>
	<div class="container" style="margin-top: 50px;">
     <form action="registerUser" method="post">
         {{ csrf_field() }}
        <div class="phone-user mt-3">
        <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><img height="30" width="30" src="{{URL::asset('images/phone.png');}}" class="img-fluid" style="background: #FF6700;box-shadow: 10px 5px 5px grey;"></span>
         </div>
         <input name="phone" type="text" class="form-control" placeholder="Please Enter a Mobile" aria-label="Username" aria-describedby="basic-addon1" style="box-shadow: 10px 5px 5px grey;" required>
         </div>
         </div>
         <div class="pass-user"> 
          <div class="input-group mb-3">
            <div class="input-group-prepend">
             <span class="input-group-text" id="basic-addon1"><img height="30" width="30" src="{{URL::asset('images/3.png');}}" class="img-fluid" style="box-shadow: 10px 5px 5px grey;"></span>
          </div>
          <input name="otp" type="text" class="form-control" placeholder="OTP" aria-label="Username" aria-describedby="basic-addon1" style="box-shadow: 10px 5px 5px grey;">
         </div>
       </div>
        
        <div class="Password"> 
          <div class="input-group mb-3">
            <div class="input-group-prepend">
             <span class="input-group-text" id="basic-addon1"><img height="30" width="30" src="{{URL::asset('images/9.jpg');}}" class="img-fluid" style="box-shadow: 10px 5px 5px grey;"></span>
          </div>
          <input name="pass" type="text" class="form-control" placeholder="Enter Password" aria-label="Username" aria-describedby="basic-addon1" style="box-shadow: 10px 5px 5px grey;" required>
         </div>
       </div>
         <div class="Recommendation"> 
          <div class="input-group mb-3 form-group ">
            <div class="input-group-prepend ">
             <span class="input-group-text" id="basic-addon1"><img height="30" width="30" src="{{URL::asset('images/8.jpg');}}" class="img-fluid" style="box-shadow: 10px 5px 5px grey;"></span>
          </div>
          <input name="refer" type="text" class="form-control  " placeholder="Referral" aria-label="Username" aria-describedby="basic-addon1" style="box-shadow: 10px 5px 5px grey;">
         </div>
       </div>
      <center><button type="submit" class="btn mt-5 sub"><h3>Register</h3></button></center>
      
</form>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>
