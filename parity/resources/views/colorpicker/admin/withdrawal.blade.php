<!DOCTYPE html>
<html>
<head>
	    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Withdrawal </title>
    
      <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin.css');}}">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <script>
        var data = "";
        var holder = {};

        function addRemoveWithdraw(check, val) {

            if (check.checked == true) {
                holder[val] = val;
            }
            else {
                delete holder[val];
            }
            
        }

        function removeWithdraw(val) {
            
        }

        function approve() {

            for(key in holder)
            {
                data += holder[key]+",";
            }

            window.location.href = "approve?dat=" + data;
        }
        function reject() {
            for(key in holder)
            {
                data += holder[key]+",";
            }
            window.location.href = "reject?dat=" + data;
        }
    </script>
</head>

<body>
	 <div class="allhead">

		  <center><img src="{{URL::asset('images/aa.png');}}" style="width:70px;"></center> 
		  <h4 class="all_arrow" type="btn" onclick="window.location.href='dashboard';"> &#8592;</h4>
	</div>
	<center class="mt-4"><h2>Withdrawal </h2></center>

   <div class="container mt-5">
   	        <button type="submit" onclick="approve()" class="btn withbutton mt-4"><h4 style="color:white;font-weight:bold;">Approve</h4></button>
   	   
   	   <button type="submit" onclick="reject()" class="btn withbutton mt-4"><h4 style="color:white;font-weight:bold;">Reject</h4></button>
   	   
   	   <table class="table table-bordered mt-5" border="1">
   	   	  <thead>
   	   	  	 <tr>
   	   	  	 	<th></th>
   	   	  	 	<th>S.No</th>
   	   	  	 	<th>User Mobile</th>
   	   	  	 	<th>Requested Amount</th>
   	   	  	 	<th>Requested Date</th>
   	   	  	 	<th>Status</th>
   	   	  	 </tr>
   	   	  </thead>
   	   	  <tbody>
   	   	  	  <?php
            use App\Models\Paymentrequest;

            $withdraws = Paymentrequest::select('*')->get();
            foreach($withdraws as $with)
            {
                echo("<tr><td><input type='checkbox' onchange='addRemoveWithdraw(this,".$with->id.")' /></td><td>".$with->id."</td><td>".$with->phone."</td><td>".$with->amount."</td><td>".$with->created_at."</td><td>".$with->status."</td></tr>");
            }
                      ?>
   	   	  </tbody>
   	   </table>
   	 
   </div>
</body>
</html>
