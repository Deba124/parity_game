<?php
session_start();
if(!isset($_SESSION['admin']))
{
    header('location:admin');
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Body</title>
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin.css');}}">
      
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
	

</head>
<body>
  <div class="container-fluid mainhead">
    
    <center><img src="{{URL::asset('images/aa.png');}}" style="width:140px;"></center>    
    
    <div class="dropdown admin">
  <a onclick="myFunction()" class="dropbtn user-admin"><i class="fas fa-user"></i><br>
     <h5>Admin</span></h5>
  <div id="myDropdown" class="dropdown-content">
    <a href="change.html">Change Password</a>
    <a href="#">Logout</a>
   </div>
  </div>
</div>
     <div class="container mt-5"> 
         <div class="col-md-12">
             <div class="row">
               <div class="col-md-4 game">
                    <a href="alluser">
                       <center>   
                          <i class="fas fa-user-friends m-2 font" style="color:white;"></i>
                           <h4><span class="ml-3" style="color:white;">All User</span></h4>
                       </center> 
                       </a>                                         
                    
                </div>
                   <div class="col-md-4  game1">
                       <a href="adduser">
                         <center>   
                            <i class="fas fa-user-plus m-2 font" style="color:white;"></i>
                            <h4><span class="ml-3"style="color:white;">Add User</span></h4>
                         </center>
                     </a>
                </div>
                 <div class="col-md-4  game2">
                     <a href="results">
                      <center>   
                          <i class="fas fa-poll m-2 font" style="color:white;"></i>
                           <h4><span class="ml-3"style="color:white;">Results</span></h4>
                       </center>
                   </a>
                </div>
                  <div class="col-md-4 game3">
                    <a href="deposit">
                      <center>   
                          <i class="fas fa-money-check m-2 font" style="color:white;"></i>
                           <h4><span class="ml-3"style="color:white;">Deposit</span></h4>
                       </center>
                   </a>
                </div>
                 <div class="col-md-4  game4">
                    <a href="withdrawal">
                     <center>   
                          <i class="fas fa-book m-2 font" style="color:white;"></i>
                           <h4><span class="ml-3"style="color:white;">Withdrawal Request</span></h4>
                       </center>
                   </a>
                </div>
                 <div class="col-md-4 game5">
                    <a href="game_management">
                    <center>   
                          <i class="fab fa-fantasy-flight-games m-2 font" style="color:white;"></i>
                           <h4><span class="ml-3"style="color:white;">Game Management</span></h4>
                       </center>
                   </a>
                </div>
               <div class="col-md-4 game6">
                  <a href="update_qr">
                   <center>   
                          <i class="fas fa-qrcode m-2 font" style="color:white;"></i>
                           <h4><span class="ml-3"style="color:white;">Update QR</span></h4>
                       </center>
                   </a>
                </div>
             </div>
         </div>
     </div>

</body>
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>

<!-- Popper JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
     </html>
