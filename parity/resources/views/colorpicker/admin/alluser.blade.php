<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>All User</title>
    
      <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin.css');}}">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
</head>
<body>
	<div class="allhead">

		  <center><img src="{{URL::asset('images/aa.png');}}" style="width:70px;"></center> 
		  <h4 class="all_arrow" type="btn" onclick="window.location.href='dashboard';"> &#8592;</h4>
	</div> 
	  <center class="mt-5"><h2>All User</h2></center>
	<div class="container mt-5">
        <table class="table table-striped table-bordered text-center">
        <thead>
        	<tr>
        	  <th>SI. No</th>
        	  <th>Phone</th>
        	    <th>Password</th> 
        	    <th>Wallet</th>
        	  
        	   <th>Status</th>
        	</tr>
             </thead>
        	<tbody>
        		
        			<?php
                    use App\Models\User;
                    $users = User::select('*')->get();
                    $count = 1;
                    foreach($users as $user)
                    {
                        echo("<tr><td>".$count."</td><td>".$user->phone."</td><td>".$user->password."</td><td>".$user->wallet."</td><td>".$user->status."</td>");

                        echo('<td><a class="btn btn-danger" href="block?u='.$user->phone.'">Block</a> &nbsp&nbsp<a class="btn btn-success" href="unblock?u='.$user->phone.'">Unblock</a></td></tr>');
                        $count++;
                    }
                    ?>
        			<!-- <td>47.00</td> --> 
        			
        		
        	</tbody>
       	 
        </table>
    
 </div>
 
</body>
</html>
