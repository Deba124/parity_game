<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>All User</title>
    
      <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin.css');}}">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
</head>
<body>
	<div class="allhead">

		  <center><img src="{{URL::asset('images/aa.png');}}" style="width:70px;"></center> 
		  <h4 class="all_arrow" type="btn" onclick="window.location.href='dashboard';"> &#8592;</h4>
	</div> 
	<div class="container mt-5">
	 <table class="table table-bordered  table-stripped text-center">
		  <thead>
		  	 <tr>
		  	 	 <th>QR</th>
		  	 	 <th>Merchant</th>
		  	 	 <th>Action</th>
		  	 	 <th>Update QR</th>
		  	 </tr>
		  </thead>
		  <tbody>
		  	  <tr>
		  	  	 <td><img src="{{URL::asset('uploads/paytm.png?type='.rand(100000,999999));}}" style="width:70px;"></td>
		  	  	 <td>Paytm</td>
		  	  	 <td><label class="switch">
                 <input type="checkbox" checked>
                 <span class="slider round"></span>
                 </label>
              </td>
              <td> <form method="post" enctype="multipart/form-data" action="updateqr">
				  {{ csrf_field() }}
                      <input type="file" id="myfile" name="Myfile" required />
				  <input type="hidden" value="paytm" name="tesla" readonly />
                      <button type="submit" class="btn btn-primary">Submit</button>
					 </form>
            </td>
		  	  
		  	  <tr>
		  	  	 <td><img src="{{URL::asset('uploads/phonepay.png?type='.rand(100000,999999));}}" style="width:70px;"></td>
		  	  	 <td>Phone Pay</td>
		  	  	 <td><label class="switch">
                 <input type="checkbox" checked>
                 <span class="slider round"></span>
                 </label>
              </td>
                 <td> <form method="post" enctype="multipart/form-data" action="updateqr">
					 {{ csrf_field() }}
                      <input type="file" id="myfile" name="Myfile" />
					 <input type="hidden" value="phonepay" name="tesla" readonly />
                      <button type="submit" class="btn btn-primary">Submit</button>
					 </form>
                  </td>
		  	  </tr>
		  	  <tr>
		  	  	 <td><img src="{{URL::asset('uploads/gpay.png?type='.rand(100000,999999));}}" style="width:70px;"></td>
		  	  	 <td>Google Pay</td>
		  	  	 <td><label class="switch">
                 <input type="checkbox" checked>
                 <span class="slider round"></span>
                 </label>
              </td>
              <td> <form method="post" enctype="multipart/form-data" action="updateqr">
				  {{ csrf_field() }}
                      <input type="file" id="myfile" name="Myfile" />
				  <input type="hidden" value="gpay" name="tesla" readonly />
                      <button type="submit" class="btn btn-primary">Submit</button>
					 </form>
              </td>
		  	  </tr>
		  </tbody>
	</table>
</div>
</body>
</html>	
