<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>All User</title>
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin.css');}}">
      <link rel="stylesheet" type="text/css" href="assets/admin.css">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <script>
		function getParams() {
			var constQuery = window.location.search;
			const urlParams = new URLSearchParams(constQuery);
			var data = urlParams.get("type");
			console.log("data is" + data);
			if (data != "general" && data != null) {
				alert(data);
            }
        }
	</script>

</head>
<body onload="getParams()">
	<div class="allhead">

		  <center><img src="{{URL::asset('images/aa.png');}}" style="width:70px;"></center> 
		  <h4 class="all_arrow" type="btn" onclick="window.location.href='dashboard';"> &#8592;</h4>
	</div>
    <center class="mt-5"><h3>Add User</h3></center>
	<div class="container" style="margin-top: 50px;">
     <form action="addUserAdmin" method="post">
         {{csrf_field()}}
        <div class="phone-user mt-3">
        <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><img height="30" width="30" src="{{URL::asset('images/phone.png');}}" class="img-fluid" style="background: #FF6700;box-shadow: 10px 5px 5px grey;"></span>
         </div>
         <input name="phone" type="text" class="form-control" placeholder="Mobile as a user Name" aria-label="Username" aria-describedby="basic-addon1" style="box-shadow: 10px 5px 5px grey;">
         </div>
         </div>
        
        <div class="Password"> 
          <div class="input-group mb-3">
            <div class="input-group-prepend">
             <span class="input-group-text" id="basic-addon1"><img height="30" width="30" src="{{URL::asset('images/9.jpg');}}" class="img-fluid" style="box-shadow: 10px 5px 5px grey;"></span>
          </div>
          <input name="pass" type="text" class="form-control" placeholder="Enter Password" aria-label="Username" aria-describedby="basic-addon1" style="box-shadow: 10px 5px 5px grey;">
         </div>
       </div>
         <div class="Recommendation"> 
          <div class="input-group mb-3 form-group ">
            <div class="input-group-prepend ">
             <span class="input-group-text" id="basic-addon1"><img height="30" width="30" src="{{URL::asset('images/8.jpg');}}" class="img-fluid" style="box-shadow: 10px 5px 5px grey;"></span>
          </div>
          <input name="refer" type="text" class="form-control  " placeholder="Referral" aria-label="Username" aria-describedby="basic-addon1" style="box-shadow: 10px 5px 5px grey;">
         </div>
       </div>
    <button type="submit" class="btn addbutton mt-4"><h3 style="color:white;font-weight: bold;">Add</h3></button>
</form>
</div>
<center>
	 
	 

</center>
 </div>
 
</body>
</html>
