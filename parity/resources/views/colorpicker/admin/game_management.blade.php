<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Game Management</title>
    
      <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin.css');}}">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
	   
</head>
<body>
      <div class="allhead">
		 <center><img src="{{URL::asset('images/aa.png');}}" style="width:70px;"></center> 
		 <h4 class="all_arrow" type="btn" onclick="window.location.href='dashboard';"> &#8592;</h4>
	</div>
     <center class="mt-4"><h2>Game Management</h2></center>
	<div class="container mt-5">
	<div class="card" style="width:70rem;">
	  <!--  <div class="row">
	   <div class="col-md-6 text-center mt-2">
		<h3>Period :</h3>
	</div>
	 <div class="col-md-6 mt-2">
		<h3 class="text-center">Time :</h3>
	</div>	
		</div>  -->
	 <table class="table  table-bordered text-center mt-5">
	 	<thead>
	 		<tr>
	 			<th>Period</th>
	 			<th>Time</th>
	 			<th>Combinations</th>
	 			
	 			
	 		</tr>
			 </thead>
	 		<tbody>
	 			<?php
				 use App\Models\Period;
				 use App\Models\Bet;
				 use App\Models\Betnumber;
				 $periods = Period::select('*')->get();
				 foreach($periods as $period)
                 {
					 $allBets = Bet::where('period',$period->period)->get();
					 $allBetsNum = Betnumber::where('period',$period->period)->get();
					 echo("<tr><td>".$period->period."</td><td>".$period->created_at."</td>");
					 echo("<td>");
					 foreach($allBets as $allBet)
                     {
                         echo($allBet->color.",");
                     }
					 foreach($allBetsNum as $allBetnum)
                     {
                         echo($allBetnum->number.",");
                     }
					 echo("</td></tr>");
                     
                 }
				 ?>
				 
	 		</tbody>
	 	
	 </table>
	 </div>
	</div>
</body>
</html>
