<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Results</title>
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin.css');}}">
      
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
	   
</head>
<body>
   <div class="allhead">
		<center><img src="{{URL::asset('images/aa.png');}}" style="width:70px;"></center> 
		<h4 class="all_arrow" type="btn" onclick="window.location.href='dashboard';"> &#8592;</h4>
	</div>
	   <center class="mt-4"><h2>Results</h2></center>
	   <div class="container mt-5">
	<div class="card" style="width:70rem;">
	   <div class="row">
	   <div class="col-md-6 text-center mt-2">
		<h3>Period :<?php
					use App\Models\Period;
					$period = Period::select('*')->orderBy('created_at','desc')->first();
					echo($period->period);
					?></h3>
	</div>
	 <div class="col-md-6 mt-2">
		<h3 class="text-center">Time :<?php
		 echo($period->created_at);
									  ?></h3>
	</div>	
		</div> 
	 <table class="table  table-bordered text-center mt-5">
	 	<thead>
	 		<tr>
	 			<th>Period</th>
	 			<th>Points Traded</th>
	 			<th>Combination</th>
	 			<th>Status</th>
	 		</tr>
			 </thead>
	 		<tbody>
	 			<?php
				 use App\Models\Result;
				 $bets = Result::select('*')->get();
				 foreach($bets as $bet)
                 {
                     echo("<tr><td>".$bet->period."</td><td>".$bet->amount."</td><td>".$bet->number.",".$bet->color."</td><td>Profit</td></tr>");
                 }
				 ?>
	 		</tbody>
	 	
	 </table>
	 </div>
	</div>
</body>
</html>	   
