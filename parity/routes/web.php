<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {

    if(isset($_COOKIE['phone']))
    {
        //dd($_COOKIE);
        $login = new Users();
        
        $data =['phone'=>$_COOKIE['phone'],'pass'=>$_COOKIE['pass']];
        $login->loginDirectly($data);
    }
    else{
        return view('colorpicker/user/login/login');
    }
   
});
Route::get('login', function () {
    return view('colorpicker/user/login/login');
});
Route::get('referal', function () {
    return view('colorpicker/user/login/referal');
});
Route::get('forget', function () {
    return view('colorpicker/user/login/forget');
});
Route::get('register', function () {
    return view('colorpicker/user/login/Register');
});
Route::get('order', function () {
    return view('colorpicker/user/layout/order');
});
Route::get('recharge', function () {
    return view('colorpicker/user/layout/recharge');
});
Route::get('banking', function () {
    return view('colorpicker/user/layout/banking');
});
Route::get('interest', function () {
    return view('colorpicker/user/layout/interest');
});

Route::get('userDash', function () {
    return view('colorpicker/user/layout/layout');
});

Route::get('withdrawalUser', function () {
    return view('colorpicker/user/layout/withdrawal');
});
Route::get('transactionUser', function () {
    return view('colorpicker/user/layout/transaction');
});
Route::get('referral', function () {
    return view('colorpicker/user/layout/referral');
});

Route::get('commision', function () {
    return view('colorpicker/user/layout/commision');
});


Route::get('adduser', function () {
    return view('colorpicker/admin/adduser');
});
Route::get('dashboard', function () {
    return view('colorpicker/admin/body');
});
Route::get('deposit', function () {
    return view('colorpicker/admin/deposit');
});
Route::get('withdrawal', function () {
    return view('colorpicker/admin/withdrawal');
});

Route::get('results', function () {
    return view('colorpicker/admin/results');
});
Route::get('game_management', function () {
    return view('colorpicker/admin/game_management');
});
Route::get('update_qr', function () {
    return view('colorpicker/admin/update_qr');
});
Route::get('alluser', function () {
    return view('colorpicker/admin/alluser');
});
Route::get('admin', function () {
    return view('colorpicker/admin/layout');
});

Route::post('registerUser',[Users::class,'UserRegistration']);
Route::post('addfund',[Users::class,'addFund']);
Route::post('loginUser',[Users::class,'login']);
Route::get('submitGame',[Users::class,'SetBet']);
Route::get('submitGameNum',[Users::class,'SetBetNumber']);
Route::get('reload',[Users::class,'reload']);
Route::get('game',[Users::class,'ShowMainGame']);
Route::get('block',[Users::class,'BlockUser']);
Route::get('unblock',[Users::class,'UnBlockUser']);
Route::post('addUserAdmin',[Users::class,'AddUser']);
Route::post('adminCheck',[Users::class,'checkAdmin']);
Route::post('saveBank',[Users::class,'saveBank']);
Route::post('withdrawAmount',[Users::class,'setWithdraw']);
Route::get('approve',[Users::class,'AcceptWithdraw']);
Route::get('reject',[Users::class,'RejectWithdraw']);
Route::post('rechargeRequest',[Users::class,'RequestRecharge']);
Route::post('updateqr',[Users::class,'resizeImage']);
